/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "2gx1FH_Fz-M",
    "titulo": "Soborno y Corrupción Parte 4 Regalos de negocios y soborno video 02",
    "descripcion": "",
    "url": "2gx1FH_Fz-M",
    "preguntas": [
        {
            "second": "10",
            "question": "¿Para ti qué es soborno?"
        }, {
            "second": "98",
            "question": "¿Sabes qué tipo de hospitalidad puedes recibir?"
        }, {
            "second": "116",
            "question": "¿Sabes que tipos de hospitalidades que no se pueden recibir?"
        }, {
            "second": "139",
            "question": "¿Conoces los MECANISMOS DE CONTROL PARA EL MANEJO DE HOSPITALIDAD, DONACIONES Y BENEFICIOS SIMILARES”?",
            "answers": ["Si","No"]
        }
    ]
}];
   

