
$(function () {

    ivo.video({
        height: '325',
        width: '640',
        videoId : videos[0].id,
        questionId: 'preguntas',
        questions: videos[0].preguntas,
        onQuestionShow: function () {
            document.getElementById('player').style.display = "none";
        },
        onCorrect: function (msg) {
            Swal.fire(
                msg,
                '',
                'success'
            )
        },
        onIncorrect: function (msg) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: msg
            });
        },
        onNextQuestion: function () {
            document.getElementById('player').style.display = "block";
            document.getElementById('preguntas').innerHTML = "";
        }
    });
});